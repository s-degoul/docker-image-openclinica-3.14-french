#!/bin/bash

BASE_DIR=$(dirname "$0")

source ${BASE_DIR}/config.sh

# Ask for confirmation unless -f parameter is provided
if [ $# -eq 0 ]
then
    while true; do
	read -p "This will overwrite existing data of openclinica${CONT_VERSION} container? Are you sure? (Y/N)" yn
	case $yn in
            [Yy]* ) break;;
            [Nn]* ) echo "Aborting"; exit 0;;
            * ) echo "Please answer Y (yes) or N (no).";;
	esac
    done
elif [ "$1" = "-f" ]
then
    echo "Restoration forced"
else
    echo "Unknown parameter $1"
    exit 1
fi

# Test if backup exist
if [ ! -d "${BASE_DIR}/backup" ]
then
    echo "backup directory not found. Exit"
    exit 1
fi
if [ ! -f "${BASE_DIR}/backup/${OC_PG_BCK_FILE}${BCK_FILE_EXT}" ]
then
    echo "Database backup file ${OC_PG_BCK_FILE}${BCK_FILE_EXT} not found in backup directory. Exit"
    exit 1
fi
if [ ! -f "${BASE_DIR}/backup/${OC_DATA_BCK_FILE}${BCK_FILE_EXT}" ]
then
    echo "Data backup file ${OC_DATA_BCK_FILE}${BCK_FILE_EXT} not found in backup directory. Exit"
    exit 1
fi

# Test if container exists
if [ ! $(docker container ls --all --format="{{.Names}}" | grep "^openclinica${CONT_VERSION}$" | wc -l) -eq 1 ]
then
    echo -e "Can not find container openclinica${CONT_VERSION}.\nPerhaps you should run create_container.sh before."
    exit 1
fi

# Test if container is started
CONT_STARTED=0
if [ $(docker container ls --format="{{.Names}}" | grep "^openclinica${CONT_VERSION}$" | wc -l) -eq 1 ]
then
    CONT_STARTED=1
fi


# Performing restoration
cd ${BASE_DIR}

# Stop running container
if [ $CONT_STARTED -eq 1 ]
then
    echo "Stopping container openclinica${CONT_VERSION}"
    docker container stop openclinica${CONT_VERSION} > /dev/null
    # Wait until container is stopped
    docker container wait openclinica${CONT_VERSION} > /dev/null
fi

echo "Restoring database"
docker container run \
       --rm  \
       --volumes-from openclinica${CONT_VERSION} \
       -v $(pwd)/backup:/tmp \
       debian:latest \
       bash -c "cd /opt/PostgreSQL/${POSTGRESQL_VERSION}/data && rm -Rf ./* && tar xzf /tmp/${OC_PG_BCK_FILE}${BCK_FILE_EXT} --strip 4"

echo "Restoring data files"
docker container run \
       --rm  \
       --volumes-from openclinica${CONT_VERSION} \
       -v $(pwd)/backup:/tmp \
       debian:latest \
       bash -c "cd /usr/local/tomcat/openclinica${CONT_VERSION}.data && rm -Rf ./* &&  tar xzf /tmp/${OC_DATA_BCK_FILE}${BCK_FILE_EXT} --strip 4"

# Start container (if it was running before restoration)
if [ $CONT_STARTED -eq 1 ]
then
    echo "Starting container openclinica${CONT_VERSION}"
    docker container start openclinica${CONT_VERSION} > /dev/null
fi

