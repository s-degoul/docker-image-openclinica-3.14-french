#!/bin/bash

BASE_DIR=$(dirname "$0")

source ${BASE_DIR}/config.sh

# Checking if image exists
if [ $(docker image ls --format="{{.Repository}}:{{.Tag}}" | grep "^openclinica:${IMG_VERSION}$" | wc -l) -eq 0 ]
then
    echo -e "Can not find Docker image openclinica:${IMG_VERSION}.\nPerhaps you should run create_image.sh before."
    exit 1
fi

# Sanitizing environment: removing already existing container
if [ $(docker container ls --all --format="{{.Names}}" | grep "^openclinica${CONT_VERSION}$" | wc -l) -eq 1 ]
then
    echo "Removing existing openclinica${CONT_VERSION} container."
    docker container stop openclinica${CONT_VERSION}
    docker container rm openclinica${CONT_VERSION}
fi

cd ${BASE_DIR}

# Creating container
docker container create \
       -p ${PORT}:8080 \
       --mount source=db.data${CONT_VERSION},target=/opt/PostgreSQL/${POSTGRESQL_VERSION}/data \
       --mount source=oc.data${CONT_VERSION},target=/usr/local/tomcat/openclinica${CONT_VERSION}.data \
       --env-file env.list  \
       --name openclinica${CONT_VERSION} \
       openclinica:${IMG_VERSION}
