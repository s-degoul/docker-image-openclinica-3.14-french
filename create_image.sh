#!/bin/bash

BASE_DIR=$(dirname "$0")

source ${BASE_DIR}/config.sh

if [ $(docker image ls --format="{{.Repository}}:{{.Tag}}" | grep "^openclinica:${IMG_VERSION}$" | wc -l) -gt 0 ]
then
    echo "Docker image openclinica:${IMG_VERSION} already exists."
    exit 1
fi

cd ${BASE_DIR}

docker build --tag="openclinica:${IMG_VERSION}" . > build_trace

