#!/bin/bash

BASE_DIR=$(dirname "$0")

source ${BASE_DIR}/config.sh

# Create backup directory
if [ ! -d "${BASE_DIR}/backup" ]
then
    mkdir ${BASE_DIR}/backup
fi

cd ${BASE_DIR}

# Test if container is started
CONT_STARTED=0
if [ $(docker container ls --format="{{.Names}}" | grep "^openclinica${CONT_VERSION}$" | wc -l) -eq 1 ]
then
    CONT_STARTED=1
fi

# Stop running container
if [ $CONT_STARTED -eq 1 ]
then
    echo "Stopping container openclinica${CONT_VERSION}"
    docker container stop openclinica${CONT_VERSION} > /dev/null
    # Wait until container is stopped
    docker container wait openclinica${CONT_VERSION} > /dev/null
fi

echo "Backup database"
docker container run \
       --rm  \
       --volumes-from openclinica${CONT_VERSION} \
       -v $(pwd)/backup:/tmp \
       debian:latest \
       tar czf /tmp/openclinica_pg_dump_$(date +%Y-%m-%d_%H-%M-%S).tar.gz -C / opt/PostgreSQL/${POSTGRESQL_VERSION}/data

echo "Backup data files"
docker container run \
       --rm  \
       --volumes-from openclinica${CONT_VERSION} \
       -v $(pwd)/backup:/tmp \
       debian:latest \
       tar czf /tmp/openclinica_data_$(date +%Y-%m-%d_%H-%M-%S).tar.gz -C / usr/local/tomcat/openclinica${CONT_VERSION}.data

# Start container (if it was running before backup)
if [ $CONT_STARTED -eq 1 ]
then
    echo "Starting container openclinica${CONT_VERSION}"
    docker container start openclinica${CONT_VERSION} > /dev/null
fi
