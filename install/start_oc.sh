#!/bin/bash

set -m

# Starting PostgreSQL
su -c "/opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/postgres -D /opt/PostgreSQL/${POSTGRESQL_VERSION}/data" ${POSTGRESQL_SUPER_USER} &
until /opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/pg_isready -U ${POSTGRESQL_SUPER_USER}
do
    sleep 1
done

# Updating database passwords
# - POSTGRESQL_VERSION, POSTGRESQL_SUPER_USER and OC_LOGIN variables available in image
# - PGPASSWORD available in the image so that PostgreSQL connexion works without providing password in commandline
# - NEW_PGPASSWORD and NEW_OC_PASSWORD variables must be provided in container creating command
/opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/psql -U ${POSTGRESQL_SUPER_USER} -c "ALTER USER ${OC_LOGIN} WITH ENCRYPTED PASSWORD '"${NEW_OC_PASSWORD}"';"
/opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/psql -U ${POSTGRESQL_SUPER_USER} -c "ALTER USER ${POSTGRESQL_SUPER_USER} WITH ENCRYPTED PASSWORD '"${NEW_PGPASSWORD}"';"
unset PGPASSWORD
unset NEW_PGPASSWORD

# Updating database password in Openclinica config file
# - OC_DB and OC_LOGIN variables available in image
# - NEW_OC_PASSWORD variable must be provided in container creating command
sed -i -e 's|<oc_db>|'${OC_DB}'|' \
    -e 's|<oc_login>|'${OC_LOGIN}'|' \
    -e 's|<oc_passwd>|'${NEW_OC_PASSWORD}'|' \
    /usr/local/tomcat/webapps/${OC_URL}/WEB-INF/classes/datainfo.properties
unset NEW_OC_PASSWORD

# Starting Tomcat
/usr/local/tomcat/bin/startup.sh

# Resuming background process (PostgreSQL)
fg %1
