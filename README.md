
[[_TOC_]]

# Overview

Dockerfile and Shell script to handle Docker image for [OpenClinica 3](https://github.com/OpenClinica/OpenClinica), an open source software for Electronic Data Capture and Clinical Data Management.

Features of OpenClinica:

- Version: 3.14
- Language: french

> Warning: knowledge about how Docker works is strongly advised.

# How to build a Docker image?

## Get required dependencies

Dependencies for image building are contained in `install` directory.

See `install/README`.

### Not provided in this repository

These dependencies have to be uploaded separately:

- Java Development Kit, named `jdk-${jdk_version}`
- Java Servlet Container Apache Tomcat, named `apache-tomcat-${tomcat_version}.tar.gz`
- `PostgreSQL`, named `postgresql-${postgresql_version}*-linux-x64.run`
- and OpenClinica binary, named `OpenClinica-3.14.zip`.

*Version variables defined in Dockerfile*

See [system requirements for OpenClinica](https://docs.openclinica.com/installation/system-requirements) to know more about recommended version of dependencies.

### Provided in this repository

- french translation files, as link provided in [french translation extension](https://community.openclinica.com/extension/french-translation) is dead resulting in trouble to get these files.
- example of configuration file for OpenClinica: `oc_datainfo.properties`. See [installation instructions](https://github.com/OpenClinica/OpenClinica/wiki/Install-OpenClinica-v3.6-and-higher-on-Linux#configure-the-openclinica-application) to know more.
- script to be launched when starting a container from the Docker image: `start_oc.sh`

## Adapt the Dockerfile

Check values of environment variables persisting in final image (`ENV`) or only intended for build-time (`ARG`), especially versions of dependencies.

Note that for security concern, variables corresponding to password are defined with dummy values. Indeed, these variables will appear in image layers (i.e. using `docker history` command).
In fact, related passwords are overwrited when creating container (see below).

## Adapt configuration variables

Check variable definition in `config.sh`.

>>>
**Beware to the value of these variables:**

- `POSTGRESQL_VERSION` have to be the same as in Dockerfile
- `PORT` should change between different versions of container intended to run at the same time
>>>

## Launch image build

Run `create_image.sh`.

Messages of build process are captured in `build_trace` file.

# How to use the image?

## Create a container

Adapt values of password in `env.list` file (renamed from `env.list.sample`). These values will overwrite dummy passwords set during image build, so they correspond to real passwords for production state.

Run `create_container.sh`.

> Warning: this will remove previous container with the same name, i.e. `openclinica${CONT_VERSION}`.

*NB: `CONT_VERSION` variable is defined in `config.sh`*

## Manage container life

Use `start.sh` and `stop.sh` scripts to start and stop container, respectively.

To remove the container, use `remove_container.sh` (with parameter `-f` to run non interactively).

> This will definitively erase the container AND related volumes (persisted data)

**Manage with systemd**

Using systemd can be convenient to handle the container in the same way as other processes of the system.

- adapt path in `docker-openclinica.service.sample`. NB: a modified version of `start.sh` script, i.e. `start_systemd.sh`, is used.
- copy this file to `/etc/systemd/system/docker-openclinica.service` (in Debian-like OS)
- change the permissions of the unit file
 ```
 sudo chmod 644 /etc/systemd/system/docker-openclinica.service
 ```
 - reload system
 ```
 sudo systemctl daemon-reload
 ```
 
 Use this service as usual: `systemctl enable docker-openclinica.service` to automatically start at boot, `systemctl start/stop docker-openclinica.service` to start/stop the container.

## Manage persistent data

These data are configured to persist in internal Docker volumes:

- database content: `db.data${CONT_VERSION}` volume
- file uploaded in OpenClinica: `oc.data${CONT_VERSION}` volume
- (logs of system? TODO)

*NB: `CONT_VERSION` variable is defined in `config.sh`*

If provided scripts (see above) are used to handle container (creation, starting and stopping), same volumes will be reused, so that persistent data are never lost.

### Backup

Use `backup_volumes.sh` script to make a backup of `db.data${CONT_VERSION}` and `oc.data${CONT_VERSION}` volumes in `backup` dir.

### Restore

Through `restore_volumes.sh` script, volumes, `db.data${CONT_VERSION}` and `oc.data${CONT_VERSION}`, are populated with backup data put (manually) in `backup` directory, named according to configuration (default: `openclinica_pg_dump.tar.gz` and `openclinica_data.tar.gz`, respectively).

**How to proceed data recovery?**

```
# Put backup of database and OpenClinica files in backup directory, e.g. most recent files from backup/ directory.
./restore_volumes.sh
```

(use parameter `-f` to run non interactively)

> Warning: this will overwrite data already present in volumes `db.data${CONT_VERSION}` and `oc.data${CONT_VERSION}`.

**How to restore apart from already running container**

- change container version in `config.sh`, e.g. by adding "_new" after version number.

```
# config.sh
CONT_VERSION="_${IMG_VERSION}_new"
```

- run `create_container.sh`
- run `restore_volumes.sh`

> This will install the system in a `openclinica${CONT_VERSION}_new` container with persistence of data in `db.data${CONT_VERSION}_new` and `db.data${CONT_VERSION}_new` volumes.

# How to access OpenClinica?

Access following URL with a web browser: `http://IP.or.domain.address.of.server:${PORT}/OpenClinica`

Credentials for first connection of administrator:

- login: root
- password: 12345678

