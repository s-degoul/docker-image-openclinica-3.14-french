# From Debian 10 stable (buster)
FROM debian:latest

# Configuration for image creation
# Last URL part (after domain name).
# /!\ Format "OpenClinica${CONT_VERSION}" required
# /!\ Must be compatible with a directory name
# /!\ Unique value required for multiple instances on a single domain name
ARG oc_url=OpenClinica_v2
# Locale of system = en_US, to prevent bug in data exportation. Cf https://forums.openclinica.com/discussion/16275/extract-data-problems and https://mailman.openclinica.com/pipermail/users/2013-February/008104.html
ARG lang=en_US
# Apache Tomcat version
ARG tomcat_version=7.0.52
# Java Development Kit (JDK) version
ARG jdk_version=8u231
# PostgreSQL version (major + minor)
ARG postgresql_version=9.5
# System user dedicated to PostgreSQL
ARG postgresql_service_user=postgres
# Super user of PostgreSQL databases : login, password
ARG postgresql_super_user=postgres
ARG postgresql_passwd=dummy_temp_passwd_1
# Database for Openclinica : name of db, login, password
ARG oc_db=openclinica
ARG oc_login=openclinica
ARG oc_passwd=dummy_temp_passwd_2

# Persistant environment variables from above configuration parameters
ENV POSTGRESQL_VERSION ${postgresql_version}
ENV POSTGRESQL_SUPER_USER ${postgresql_super_user}
ENV PGPASSWORD ${postgresql_passwd}
ENV OC_LOGIN ${oc_login}
ENV OC_DB ${oc_db}
ENV OC_URL ${oc_url}
ENV LANG ${lang}.UTF-8
ENV LANGUAGE ${lang}.UTF-8
ENV LC_ALL ${lang}.UTF-8
# For Java
ENV JAVA_HOME /usr/local/java
# Prevent Java PermGen outbound error
# Try with 256 256 180? cf. https://en.wikibooks.org/wiki/OpenClinica_User_Manual/OutOfMemoryError
ENV JAVA_OPTS "-Xms512m -Xmx512m -XX:PermSize=512m"

# Labels of image
LABEL description="Openclinica 3.14 based on Debian buster"
LABEL maintainer="S Degoul <samuel.degoul@ghrmsa.fr>"

# Creating installation directory
RUN mkdir -p /usr/local/oc/install
WORKDIR /usr/local/oc/install

# Loading required binaries
COPY install/jdk-${jdk_version}* /usr/local/oc/install
COPY install/apache-tomcat-${tomcat_version}.tar.gz /usr/local/oc/install
COPY install/postgresql-${postgresql_version}*-linux-x64.run /usr/local/oc/install
COPY install/OpenClinica-3.14.zip /usr/local/oc/install

# Installing required dependances
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests \
	       unzip \
	       tar \
	       locales \
    && rm -rf /var/lib/apt/lists/*

# Configuring locales
RUN echo "Europe/Paris" > /etc/timezone \
    && dpkg-reconfigure -f noninteractive tzdata \
    && sed -i -e "s/# ${lang}.UTF-8 UTF-8/${lang}.UTF-8 UTF-8/" /etc/locale.gen \
    && echo "LANG=${lang}.UTF-8">/etc/default/locale \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=${lang}.UTF-8

# Installing JDK
RUN tar -zxvf jdk-${jdk_version}* \
    && mv jdk1* /usr/local/ \
    && ln -s /usr/local/jdk1* /usr/local/java

# Installing Tomcat
RUN tar -zxvf apache-tomcat-* \
    && rm apache-tomcat-${tomcat_version}.tar.gz \
    && mv apache-tomcat-* /usr/local/ \
    && ln -s /usr/local/apache-tomcat-${tomcat_version} /usr/local/tomcat \
    && /usr/sbin/useradd -M tomcat \
    && mkdir /usr/local/tomcat/oldwebapps \
    && mv /usr/local/tomcat/webapps/* /usr/local/tomcat/oldwebapps

# Installing PostgreSQL
RUN chmod a+x postgresql-${POSTGRESQL_VERSION}* \
    && ./postgresql-${POSTGRESQL_VERSION}* --mode unattended \
		    --unattendedmodeui minimalWithDialogs \
		    --debuglevel 4 \
		    --disable-stackbuilder 1 \
		    --superaccount ${POSTGRESQL_SUPER_USER} \
		    --serviceaccount ${postgresql_service_user} \
		    --prefix /opt/PostgreSQL/${POSTGRESQL_VERSION} \
		    --datadir /opt/PostgreSQL/${POSTGRESQL_VERSION}/data \
		    --superpassword ${PGPASSWORD} \
		    --serverport 5432 \
    && /opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/psql -U ${POSTGRESQL_SUPER_USER} -c "CREATE ROLE ${OC_LOGIN} LOGIN ENCRYPTED PASSWORD '"${oc_passwd}"' SUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE" \
    && /opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/psql -U ${POSTGRESQL_SUPER_USER} -c "CREATE DATABASE ${OC_DB} WITH ENCODING='UTF8' OWNER=${OC_LOGIN}" \
    && /opt/PostgreSQL/${POSTGRESQL_VERSION}/bin/psql -U ${POSTGRESQL_SUPER_USER} -c "CREATE DATABASE randi WITH ENCODING='UTF8' OWNER=${OC_LOGIN}"

# Installing Openclinica
RUN unzip OpenClinica-3.14.zip \
    && cd OpenClinica-3.14/distribution/ \
    && unzip OpenClinica.war -d ${oc_url} \
    && cp -R ${oc_url} /usr/local/tomcat/webapps
# Configuration file
COPY install/oc_datainfo.properties /usr/local/tomcat/webapps/${oc_url}/WEB-INF/classes/datainfo.properties
# French translation
ADD install/french_translation.tar.gz /usr/local/tomcat/webapps/${oc_url}/WEB-INF/classes/org/akaza/openclinica/i18n
# Correction of bug of icon displays in CRF monitor view
RUN sed -i -e 's|<c:out value="${contextPath}" />/||' /usr/local/tomcat/webapps/${oc_url}/WEB-INF/jsp/submit/showItemInputMonitor.jsp \
    && sed -i -e 's|<c:out value="${contextPath}" />/||' /usr/local/tomcat/webapps/${oc_url}/WEB-INF/jsp/submit/showGroupItemInputMonitor.jsp

RUN chown -R tomcat /usr/local/tomcat/* \
    && chown -R tomcat /usr/local/apache-tomcat-*

# Installing initiation script
COPY install/start_oc.sh /usr/bin/start_oc
RUN chmod +x /usr/bin/start_oc

EXPOSE 8080

CMD ["/usr/bin/start_oc"]

