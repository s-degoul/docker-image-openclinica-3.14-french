#!/bin/bash

# PostgreSQL version (major + minor)
# Have to be the same as in Dockerfile!
POSTGRESQL_VERSION=9.5

# Docker image
IMG_VERSION=v2

# Container version
# DO NOT CHANGE
CONT_VERSION="_${IMG_VERSION}"  # Tips: change ${oc_url} in Dockerfile accordingly!

# Host port. Change it between different image versions!
PORT=8080

# Name of database backup file
OC_PG_BCK_FILE=openclinica_pg_dump
# Name of data (files) backup file
OC_DATA_BCK_FILE=openclinica_data
# Extension of backup files
BCK_FILE_EXT=.tar.gz

