#!/bin/bash

BASE_DIR=$(dirname "$0")

source ${BASE_DIR}/config.sh

docker container stop openclinica${CONT_VERSION}
