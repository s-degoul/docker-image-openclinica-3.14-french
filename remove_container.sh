#!/bin/bash

BASE_DIR=$(dirname "$0")

source ${BASE_DIR}/config.sh

# Ask for confirmation unless -f parameter is provided
if [ $# -eq 0 ]
then
    while true; do
	read -p "This will definitively delete openclinica${CONT_VERSION} container and its volumes (data)? Are you sure? (Y/N)" yn
	case $yn in
            [Yy]* ) break;;
            [Nn]* ) echo "Aborting"; exit 0;;
            * ) echo "Please answer Y (yes) or N (no).";;
	esac
    done
elif [ "$1" = "-f" ]
then
    echo "Deletion forced"
else
    echo "Unknown parameter $1"
    exit 1
fi

# Test if container exists
if [ ! $(docker container ls --all --format="{{.Names}}" | grep "^openclinica${CONT_VERSION}$" | wc -l) -eq 1 ]
then
    echo -e "Can not find container openclinica${CONT_VERSION}.\nAborting."
    exit 1
fi

# Stop container is started
docker container stop openclinica${CONT_VERSION} > /dev/null

# Deleting container
echo "Deleting openclinica${CONT_VERSION} container"
docker container rm openclinica${CONT_VERSION} > /dev/null

# Deleting volumes
echo "Deleting db.data${CONT_VERSION} and oc.data${CONT_VERSION} volumes"
docker volume rm db.data${CONT_VERSION} > /dev/null
docker volume rm oc.data${CONT_VERSION} > /dev/null
